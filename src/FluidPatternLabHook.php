<?php
declare(strict_types=1);

namespace JMVS\FluidPatternEngineExport;

use NamelessCoder\FluidPatternEngine\Resolving\PartialNamingHelper;
use NamelessCoder\FluidPatternEngine\Traits\FluidLoader;
use PatternLab\Config;
use PatternLab\PatternData;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use TYPO3Fluid\Fluid\Core\Parser\SyntaxTree\NodeInterface;

class FluidPatternLabHook
{
    use FluidLoader;

    const FLUX_PARTIAL = '                         
                         <flux:form.option.group value="{f:translate(key:\'group.patternLab\', default:\'[Pattern Lab]\')}"/>
                         <flux:form.option.icon value="EXT:%s/Resources/Public/Icons/%s.svg"/>
                                      
                         %s
                         ';
    const FLUX_WRAPPER = '<div xmlns="http://www.w3.org/1999/xhtml" lang="en"
                  xmlns:f="http://typo3.org/ns/TYPO3/Fluid/ViewHelpers"
                  xmlns:flux="http://typo3.org/ns/FluidTYPO3/Flux/ViewHelpers"
             >
                 <f:layout name="Content"/>
                 
                 <f:section name="Documentation">
                    <f:comment>
                        %s
                    </f:comment>
                 </f:section>
             
                 <f:section name="Configuration">
                     <flux:form id="%s">

                         <f:variable name="component">%s</f:variable>
                         <f:render partial="FluxForms/Fields/Preview" arguments="{_all}"/>
                         <c:render fluxForm="%s" arguments="{_all}" />
                     </flux:form>
                 </f:section>
             
             

                 
                 <f:section name="Main">
                        <c:render component="%s" arguments="{arguments}" />
                 </f:section>
             </div>';

    const LANGUAGE_WRAPPER = '<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<xliff version="1.0">
	<file source-language="en" datatype="plaintext" original="messages" date="2019-03-08T09:45:53Z">
		<header>
			<generator>LFEditor</generator>
		</header>
		<body>
			%s
		</body>
	</file>
</xliff>
    ';

    const LANGUAGE_FIELD = '<trans-unit id="flux.%s">
				<source>%s</source>
			</trans-unit>
    ';

    public function getListeners()
    {
        return [
            'builder.generatePatternsEnd' => [
                'callable' => static::class . '::generatePatternsEnd'
            ]
        ];
    }

    public function generatePatternsEnd(Event $event, string $eventName, EventDispatcherInterface $eventDispatcher)
    {
        $this->copyPatternSourceFiles();
    }

    protected function copyPatternSourceFiles()
    {
        $data = PatternData::get();

        $targetDirectory = Config::getOption('fluidTYPO3ExtensionExport.path');

        if (!$targetDirectory) {
            throw new \RuntimeException('Configuration option "fluidTYPO3ExtensionExport.path" must be set to a valid path');
        }
        $targetDirectory = realpath($targetDirectory);
        if (!$targetDirectory || !is_dir($targetDirectory)) {
            mkdir(Config::getOption('fluidTYPO3ExtensionExport.path'), 0755, true);
        }

        $filtered = array_filter($data, function (array $item) {
            return $item['category'] === 'pattern';
        });

        $types = array_filter($data, function (array $item) {
            return $item['category'] !== 'pattern';
        });

        $helper = new PartialNamingHelper();
        $parser = $this->view->getRenderingContext()->getTemplateParser();
        $allLanguageFields = '';
        foreach ($filtered as $patternName => $patternConfiguration) {
            $languageFields = '';

            if (array_key_exists('pseudo', $patternConfiguration) && $patternConfiguration['pseudo']) {
                continue;
            }


            $targetFilename = $helper->determineTargetFileLocationForPattern($patternName);
            $sourceFilename = $this->determineSourceFileLocation($patternConfiguration, $types);
            if (!file_exists($sourceFilename)) {
                throw new \UnexpectedValueException('File "' . $sourceFilename . '" was referenced by pattern "' . $patternName . '" but the file does not exist');
            }
            $targetPatternDirectory = pathinfo($targetFilename, PATHINFO_DIRNAME);
            if (!is_dir($targetPatternDirectory)) {
                mkdir($targetPatternDirectory, 0755, true);
            }

            $source = file_get_contents($sourceFilename);
            $finalSource = $source;

            // Parse the file, creating a ParsingState, enabling us to read the layout name if one was used.
            // If a layout is used, make sure this file also gets copied to the output folder - and rename
            // it to proper UpperCamelCase in both file name and f:layout statement.
            $parsedTemplate = $parser->parse($finalSource);
            if ($parsedTemplate->hasLayout()) {
                // Rewrite the target filename to replace the layout node. Do this by evaluating, so in case any
                // dynamic Layout naming is used, this gets hardcoded in the export.
                $layoutName = $parsedTemplate->getLayoutName($this->view->getRenderingContext());
                if ($layoutName instanceof NodeInterface) {
                    $layoutName = $layoutName->evaluate($this->view->getRenderingContext());
                }
                $properLayoutName = ucfirst($layoutName);
                if (strpos($properLayoutName, '_') || strpos($properLayoutName, '-')) {
                    // Layout is using invalid name parts - convert to UpperCamelCase variant.
                    $parts = preg_split('/[_\\-]+/', $properLayoutName);
                    $parts = array_map('ucfirst', $parts);
                    $properLayoutName = implode('', $parts);
                }

                $layoutMatchPattern = '/f:layout(\\s*name=([\'"])(' . $layoutName . ')[\'"])?/';
                $matches = [];
                preg_match($layoutMatchPattern, $finalSource, $matches);
                $finalSource = $this->writeNewLayoutName($finalSource, $layoutName, $properLayoutName);

                $targetLayoutDirectory = $targetDirectory . '/Resources/Private/Layouts';
                if (!is_dir($targetLayoutDirectory)) {
                    mkdir($targetLayoutDirectory, 0755, true);
                }

                $layoutSourceFilename = $this->view->getRenderingContext()->getTemplatePaths()->getLayoutPathAndFilename($layoutName);
                $layoutTargetFilename = $targetLayoutDirectory . '/' . $properLayoutName . '.html';

                // Rewrite the layout source just in case it also contains an f:layout node. While layouts are
                // not required to contain this node, and it does not get evaluated, we rewrite in order to
                // reduce potential confusion.
                $layoutSource = file_get_contents($layoutSourceFilename);
                $layoutSource = $this->writeNewLayoutName($layoutSource, $layoutName, $properLayoutName);
                file_put_contents($layoutTargetFilename, $layoutSource);
            }

            // Next, identify any "f:render" statements which render partials (with or without sections). Rewrite all
            // of those to use the expected target partial naming.
            $finalSource = preg_replace_callback('/(f:render.+partial=["\'])([^"\']+)/', function (array $matches) {
                return $matches[1] . (new PartialNamingHelper())->determinePatternSubPath($matches[2]);
            }, $finalSource);

            // If there is data, it maybe is a component
            if (isset($patternConfiguration['data'])) {
                if (Config::getOption('fluidTYPO3ExtensionExport.exportDocumentation')) {
                    $this->exportDocumentation($targetFilename, $sourceFilename);
                }
                if (Config::getOption('fluidTYPO3ExtensionExport.exportData')) {
                    $this->exportData($targetFilename, $patternConfiguration, $types);
                }


                if (Config::getOption('fluidTYPO3ExtensionExport.generateFluxFields')) {
                    $this->generateFluxFields($targetFilename, $patternConfiguration, $types,
                        $patternName);
                }

                if (Config::getOption('fluidTYPO3ExtensionExport.generateBasicScaffoldingForContentElements')) {
                    $this->generateBasicScaffoldingForContentElements($targetFilename, $patternConfiguration, $types,
                        $patternName);
                }

                if (Config::getOption('fluidTYPO3ExtensionExport.generateBasicLanguageFile')) {
                    $languageFields = $this->generateLanguageValues($targetFilename, $patternConfiguration, $types,
                        $patternName);
                    $allLanguageFields .= $languageFields;
                    $key = $this->getKeyFromPatternString($patternName);

                    $this->writeLanguageFile($languageFields, $key);

                }
                if (Config::getOption('fluidTYPO3ExtensionExport.generateFluidParametersFromJsonAttributes')) {
                    $finalSource = $this->generateFluidParametersFromJsonAttributes($finalSource,
                        $patternConfiguration['data']);
                }
            }

            file_put_contents($targetFilename, $finalSource);
        }
        if (Config::getOption('fluidTYPO3ExtensionExport.generateBasicLanguageFile')) {
            $this->writeLanguageFile($allLanguageFields);
        }

        return null;
    }

    protected function writeNewLayoutName(string $source, string $oldLayoutName, string $newLayoutName): string
    {
        $layoutMatchPattern = '/f:layout(\\s*name=([\'"])(' . $oldLayoutName . ')[\'"])?/';
        $matches = [];
        preg_match($layoutMatchPattern, $source, $matches);
        if (!isset($matches[2])) {
            // Source contains a *NAMED* layout. Nodes that do not specify name="" are ignored since they do not need
            // to be rewritten!
            return $source;
        }
        return preg_replace($layoutMatchPattern, 'f:layout name=' . $matches[2] . $newLayoutName . $matches[2],
            $source);
    }

    protected function determineSourceFileLocation(array $patternConfiguration, array $types): string
    {
        $fileSubpath = ($patternConfiguration['pathName'] ?: $patternConfiguration['path'] . DIRECTORY_SEPARATOR . $patternConfiguration['name']) . '.' . $patternConfiguration['ext'];
        $type = null;
        foreach ($types as $type) {

            if (isset($type['name']) && isset($patternConfiguration['type']) && ($type['name'] === $patternConfiguration['type'])) {
                break;
            }
        }
        return $type['path'] . DIRECTORY_SEPARATOR . $fileSubpath;
    }

    protected function determineSourceFolder(array $patternConfiguration, array $types): string
    {
        $fileSubpath = $patternConfiguration['path'];
        $type = null;
        foreach ($types as $type) {

            if (isset($type['name']) && isset($patternConfiguration['type']) && ($type['name'] === $patternConfiguration['type'])) {
                break;
            }
        }
        return $type['path'] . DIRECTORY_SEPARATOR . $fileSubpath;
    }

    /**
     * @param $folder
     * @return array
     */
    protected function extractData($folder, $merge = false)
    {
        $pathObjects = [];
        $pathObject = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($folder),
            \RecursiveIteratorIterator::SELF_FIRST
        );
        $pathObject->setFlags(\FilesystemIterator::SKIP_DOTS);
        $pathObjects[$folder] = $pathObject;
        $data = [];
        foreach ($pathObjects as $objects) {
            $objects = iterator_to_array($objects);
            /** @var \SplFileInfo $object */
            foreach ($objects as $object) {
                if ($object->isFile() && $object->getExtension() === 'json') {
                    if ($merge) {
                        $data = array_merge_recursive($data,
                            json_decode(file_get_contents($object->getRealPath()), true));
                        foreach ($data as $key => $value) {
                            if (is_array($value)) {
                                $data[$key] = array_unique($value);
                                $multiSelectFields = Config::getOption('fluidTYPO3ExtensionExport.maps.multiSelect.columns');
                                if (in_array($key, $multiSelectFields)) {
                                    $data[$key] = array_unique(explode(" ", implode(" ", $data[$key])));
                                }
                            }
                        }
                    } else {
                        $data[] = json_decode(file_get_contents($object->getRealPath()), true);
                    }
                }
            }
        }

        return $data;
    }

    /**
     * @param $param
     * @param $defaultValue
     * @param $fluidComponentWrapper
     * @return string
     */
    protected function appendsParameter($param, $defaultValue, $fluidComponentWrapper)
    {
        $fluidComponentWrapper .= "\t" . '<f:parameter name="' . $param . '" type="' . gettype($defaultValue) . '" 
    required="0" default="' . $defaultValue . '"   description="' . $defaultValue . '" />' . "\n";
        return $fluidComponentWrapper;
    }

    /**
     * @param $params
     * @param $fluidComponentWrapper
     * @return string
     */
    protected function generateParameters($params, $fluidComponentWrapper)
    {
        foreach ($params as $param => $defaultValue) {
            if (is_array($defaultValue)) {
                $fluidComponentWrapper = $this->generateParameters($defaultValue, $fluidComponentWrapper);
            } else {
                $fluidComponentWrapper = $this->appendsParameter($param, $defaultValue, $fluidComponentWrapper);
            }
        }
        return $fluidComponentWrapper;
    }


    /**
     * @param $finalSource
     * @param $params
     */
    protected function generateFluidParametersFromJsonAttributes($finalSource, $params = [])
    {
        $fluidComponentWrapper = '';
        if (!empty($params)) {
            $fluidComponentWrapper = "<f:comment>Parameters START</f:comment>\n\n";
            $fluidComponentWrapper = $this->generateParameters($params, $fluidComponentWrapper);
            $fluidComponentWrapper .= "\n<f:comment>Parameters END</f:comment>\n\n";
        }
        $fluidComponentWrapper .= $finalSource;
        $fluidComponentWrapper .= "\n";

        return $fluidComponentWrapper;
    }

    /**
     * @param $targetFilename
     * @param $patternConfiguration
     * @param $types
     */
    protected function exportData($targetFilename, $patternConfiguration, $types)
    {
        $folder = $this->determineSourceFolder($patternConfiguration, $types);
        $dataFromFiles = $this->extractData($folder);
        file_put_contents(str_replace('.html', '.json', $targetFilename), json_encode([
            'data' => $dataFromFiles
        ], JSON_PRETTY_PRINT));
    }

    protected function determineFieldType($key)
    {
        $maps = Config::getOption('fluidTYPO3ExtensionExport.maps');
        foreach ($maps as $type => $config) {
            if (in_array($key, $config['columns'])) {
                return "<f:if condition=\"{disabled.$key} == false\">\n%s\n" . $config['render'] . "</f:if>";
            }
        }
        return "<f:if condition=\"{disabled.$key} == false\">\n%s\n" . $maps['input']['render'] . "</f:if>";
    }

    protected function prepareItems($id, $name, $items)
    {
        if (empty($items) || is_array($items) == false) {
            return ['', "items=\"{ \n" . sprintf("%s:{0:'%s',1:'%s'} \n", 0, 'None', 'null') . "}\" \n"];
        }
        $itemLabels = '';
        $result = 'items="{' . "\n";
        $i = 0;

        $labelHelper = "<f:variable name=\"%s\"><f:translate key=\"%s\" /></f:variable>\n";


        foreach ($items as $item) {
            if (empty($item) == false) {

                $labelId = $id . '_' . $name . '_' . $item;
                $labelPath = "LLL:EXT:civitas_contentelements/Resources/Private/Language/locallang.xlf:flux.{component}.fields.arguments.$name.$item";
                $itemLabels .= sprintf($labelHelper, $labelId, $labelPath);

                $label = "{$labelId}";
                $value = $item;
                $resulItem = sprintf("%s:{0:%s,1:'%s'},\n", $i, $label, $value);
                $result .= $resulItem;
            }
            $i++;
        }
        $result = substr($result, 0, -2) . "\n";
        $result .= '}"' . "\n";
        return [$itemLabels, $result];
    }

    /**
     * @param $targetFilename
     * @param $patternConfiguration
     * @param $types
     */
    protected function generateBasicScaffoldingForContentElements(
        $targetFilename,
        $patternConfiguration,
        $types,
        $patternName
    ) {
        $folder = $this->determineSourceFolder($patternConfiguration, $types);
        $properties = $this->extractData($folder, true);
        $key = $this->getKeyFromPatternString($patternName);
        $documentation = '';
        $docPath = str_replace('.html', '.md', $targetFilename);
        if (file_exists($docPath)) {
            $documentation = file_get_contents($docPath);
        }
        $renderWrapper = sprintf(self::FLUX_WRAPPER, $documentation, lcfirst($key), lcfirst($key), $key, $key);

        $path = str_replace('Partials/Components/',
            'Templates/Content/', $targetFilename);
        $path = str_replace('/' . $key . '.html',
            '.html', $path);
        if (strpos($path, 'Content') !== false) {
            $path = str_replace(Config::getOption('fluidTYPO3ExtensionExport.export_extension_key'),
                Config::getOption('fluidTYPO3ExtensionExport.contents_extension_key'), $path);
        }

        file_put_contents($path, $renderWrapper);

    }

    /**
     * @param $targetFilename
     * @param $patternConfiguration
     * @param $types
     */
    protected function generateFluxFields(
        $targetFilename,
        $patternConfiguration,
        $types,
        $patternName
    ) {
        $folder = $this->determineSourceFolder($patternConfiguration, $types);
        $properties = $this->extractData($folder, true);
        $key = $this->getKeyFromPatternString($patternName);

        $render = '';
        $labelTag = '';
        if (strpos($targetFilename, 'Components') > 0) {


            $fluxFormPath = str_replace(['Partials/Components/', $key . '/' . $key], ['Partials/FluxForms/', $key],
                $targetFilename);
            $fluxDir = str_replace('/' . $key . '.html', '', $fluxFormPath);

            if (!is_dir($fluxDir) && strpos($fluxFormPath, 'FluxForms') > 0) {
                mkdir($fluxDir, 0755, true);

            }


            if (!is_dir($fluxDir . '/Fields/') && strpos($fluxDir, 'FluxForms') > 0) {
                mkdir($fluxDir . '/Fields/', 0755, true);

            }

            if (!is_dir($fluxDir . '/Fields/' . $key . '/') && strpos($fluxDir, 'FluxForms') > 0) {
                mkdir($fluxDir . '/Fields/' . $key . '/', 0755, true);

            }


            foreach ($properties as $property => $value) {
                $fluxField = $this->determineFieldType($property);


                $labelHelper = "<f:variable name=\"%s\"><f:translate key=\"%s\" /></f:variable>\n";
                //$fluxField = $labelHelper . $fluxField;
                $labels = '';
                $items = '';
                if (strpos($fluxField, 'select') > 0) {


                    list(
                        $labels, $items) = $this->prepareItems(lcfirst($key), $property, $value);


                }
                $ceLabels = sprintf($labelHelper,
                        'label_' . $property,
                        'LLL:EXT:civitas_contentelements/Resources/Private/Language/locallang.xlf:flux.{component}.fields.arguments.' . $property) . $labels;
                $labelTag .= $ceLabels;
                $completeField = sprintf($fluxField, $ceLabels,
                        $property,
                        '{label_' . $property . '}', $items) . "\n";

                $render .= '<c:render fluxField="' . ucfirst($property) . '" fluxForm="{component}" arguments="{_all}" />';

                file_put_contents($fluxDir . '/Fields/' . $key . '/' . ucfirst($property) . '.html',
                    $completeField);

            }


            $renderProperties = sprintf(self::FLUX_PARTIAL,
                Config::getOption('fluidTYPO3ExtensionExport.contents_extension_key'), $key, $render);


            file_put_contents($fluxFormPath,
                $renderProperties);

            file_put_contents(str_replace('.html', '.json', $fluxFormPath),
                json_encode($properties, JSON_PRETTY_PRINT));
        }


    }

    /**
     * @param $targetFilename
     * @param $patternConfiguration
     * @param $types
     */
    protected function generateLanguageValues(
        $targetFilename,
        $patternConfiguration,
        $types,
        $patternName
    ) {
        $folder = $this->determineSourceFolder($patternConfiguration, $types);
        $properties = $this->extractData($folder, true);
        $aux = explode('-', $patternName);
        $text = implode(' ', $aux);

        $type = array_shift($aux);
        $parts = array_map('ucfirst', $aux);
        $key = implode('', $parts);
        $languageFields = '';

        if ($type == 'components') {
            $languageFields .= sprintf(self::LANGUAGE_FIELD, lcfirst($key),
                    implode(' ', array_map('ucfirst', $aux))) . "\n";
            $languageFields .= sprintf(self::LANGUAGE_FIELD, lcfirst($key) . '.description',
                    'Descripton for ' . str_replace('components', 'component', $text)) . "\n";
            foreach ($properties as $property => $value) {
                $languageFields .= sprintf(self::LANGUAGE_FIELD, lcfirst($key) . '.fields.arguments.' . $property,
                        implode(' ', array_map('ucfirst', explode(' - ', $property)))) . "\n";
            }

        }

        return $languageFields;

    }

    /**
     * @param $targetFilename
     * @param $sourceFilename
     */
    protected function exportDocumentation($targetFilename, $sourceFilename)
    {
        $documentationPath = str_replace('.fluid', '.md', $sourceFilename);
        if (file_exists($documentationPath)) {
            $documentation = file_get_contents($documentationPath);
            file_put_contents(str_replace('.html', '.md', $targetFilename), $documentation);
        }
    }

    /**
     * @param $targetFilename
     * @param $languageFields
     * @param $key
     */
    protected function writeLanguageFile($languageFields, $componentName = false)
    {
        $languageFileContent = sprintf(self::LANGUAGE_WRAPPER, $languageFields);
        $directory = realpath(Config::getOption('fluidTYPO3ExtensionExportPath'));
        if ($componentName) {
            file_put_contents($directory . DIRECTORY_SEPARATOR . 'Resources/Private/Language/FluxForms/' . $componentName . '.xlf',
                $languageFileContent);
        } else {
            file_put_contents($directory . DIRECTORY_SEPARATOR . 'Resources/Private/Language/locallang.xlf',
                $languageFileContent);
        }


    }

    /**
     * @param $patternName
     * @return string
     */
    protected function getKeyFromPatternString($patternName)
    {
        $parts = array_map('ucfirst', explode('-', $patternName));
        $type = array_shift($parts);
        $key = implode('', $parts);
        return $key;
    }
}
